"use strict";

/**
 * Function to set up event listeners and handle user interactions for the tile selection.
 *
 * @function
 */
document.addEventListener("DOMContentLoaded", function () {
    /** @const {Element} */
    const sizeChoice = document.getElementById("numberChoice");
    const gameBoard = document.querySelector("#game-board");
    const startGameButton = document.getElementById("start-game");
    const submit = document.getElementById("submit");
    /** @type {number} */
    let numSelected = 0;
    const gameMessage = document.createElement("figcaption");
    /** @type {number} */


    if (gameBoard) {
    // Add a click event listener for the "Start the game!" button
        startGameButton.addEventListener("click", function () {
            createTable();
        });
    }

    /**
     * Function to create the game board and handle user interactions during the game.
     *
     * @function
     */
    function createTable() {
        const size = parseInt(sizeChoice.value);
        if (isNaN(size)) {
            return;
        }

        // Clear the existing table
        gameBoard.textContent = undefined;
        const colorChoice = document.getElementById("colorChoice").value;
        numSelected = 0;
        // Create a new table
        let correctCounter = 0;
        const difficulty = document.getElementById("difficultySlider").value;
        const table = document.createElement("table");
        const cellWidth = 100 / size;

        for (let i = 0; i < size; i++) {
            const row = document.createElement("tr");

            for (let j = 0; j < size; j++) {
                const cell = document.createElement("td");
                cell.style.width = cellWidth + "%";
                row.appendChild(cell);

                const [red, green, blue] = generateRandomRGB(difficulty);
                const rgb = `rgb(${red}, ${green}, ${blue})`;
                cell.style.backgroundColor = rgb;

                if (isCorrectColor(red, green, blue)) {
                    cell.classList.add("correctTile");
                    correctCounter++;
                }

                const rgbDiv = document.createElement("div");
                const predominantColor = getPredominantColor(red, green, blue);
                rgbDiv.textContent = `rgb: (${red}, ${green}, ${blue}) ${predominantColor}`;
                cell.appendChild(rgbDiv);

                /**
                 * Event listener for toggling the state of a tile.
                 * @function
                 * @param {MouseEvent} e - The mouse event object.
                 */
                cell.addEventListener("click", function () {
                    toggleCellSelection(cell);
                });
            }

            table.appendChild(row);
        }
        gameBoard.appendChild(table);
        gameMessage.textContent = `Searching for ${colorChoice} tiles.` +
        `Your target is ${correctCounter} tiles.` +
        `You clicked ${numSelected} tiles!`;
        gameBoard.appendChild(gameMessage);

        /**
         * Event listener for the Shift + C cheat code to toggle the visibility of tile text.
         * @function
         * @param {KeyboardEvent} e - The keyboard event object.
         */
        document.addEventListener("keydown", function (e) {
            if (e.shiftKey && e.key === "C") {
                toggleTileTextVisibility();
            }
        });

        let tilesHidden = true;

        /**
         * Function to toggle the visibility of tile text.
         *
         * @function
         */
        function toggleTileTextVisibility() {
            const tileTexts = document.querySelectorAll("#game-board td div");

            tileTexts.forEach(function (textDiv) {
                textDiv.style.visibility = tilesHidden ? "visible" : "hidden";
            });
            tilesHidden = !tilesHidden;
        }

        /**
         * Function to toggle cell selection and update the game message.
         *
         * @function
         * @param {Element} cell - The clicked cell.
         */
        function toggleCellSelection(cell) {
            cell.classList.toggle("selected");
            if (cell.classList.contains("selected")) {
                numSelected++;
            } else {
                numSelected--;
            }
            updateGameMessage();
        }

        /**
         * Function to update the game message.
         *
         * @function
         */
        function updateGameMessage() {
            gameMessage.textContent = `Searching for ${colorChoice} tiles.` + 
            `Your target is ${correctCounter} tiles.` +
            `You clicked ${numSelected} tiles!`;
        }

        /**
         * Function to generate random RGB values based on the difficulty level.
         *
         * @function
         * @param {number} difficulty - The difficulty level.
         * @returns {Array} An array containing [red, green, blue] values.
         */
        function generateRandomRGB(difficulty) {
            const maxColorValue = 255;
            let range;
            if (difficulty === "1") {
                range = 80;
            } else if (difficulty === "2") {
                range = 40;
            } else if (difficulty === "3") {
                range = 10;
            } else {
                // Default to full range for difficulty level 0
                range = maxColorValue;
            }

            /* Does not work properly for other difficulties */
            const red = Math.floor(Math.random() * (maxColorValue + 1));
            const minGreen = Math.max(0, red - range);
            const maxGreen = Math.min(maxColorValue, red + range);
            const green = Math.floor(Math.random() * (maxGreen - minGreen + 1) + minGreen);

            const minBlue = Math.max(0, red - range, green - range);
            const maxBlue = Math.min(maxColorValue, red + range, green + range);
            const blue = Math.floor(Math.random() * (maxBlue - minBlue + 1) + minBlue);
            return [red, green, blue];
        }

        /**
         * Determines if the given color values match the chosen color choice.
         * @function
         * @param {number} red - The red component of the color.
         * @param {number} green - The green component of the color.
         * @param {number} blue - The blue component of the color.
         * @returns {boolean} - True if the color matches the chosen color choice; otherwise, false.
         */
        function isCorrectColor(red, green, blue) {
            if (colorChoice === "red") {
                return red > green && red > blue;
            } else if (colorChoice === "green") {
                return green > red && green > blue;
            } else if (colorChoice === "blue") {
                return blue > red && blue > green;
            }
            return false;
        }

        /**
         * Determines the predominant color of the given RGB values.
         * @function
         * @param {number} red - The red component of the color.
         * @param {number} green - The green component of the color.
         * @param {number} blue - The blue component of the color.
         * @returns {string} - The predominant color ("Red," "Green," "Blue," or "None").
         */
        function getPredominantColor(red, green, blue) {
            if (red > green && red > blue) {
                return "Red";
            } else if (green > red && green > blue) {
                return "Green";
            } else if (blue > red && blue > green) {
                return "Blue";
            } else {
                // In case of a tie or when values are equal
                return "None"; 
            }
        }

        /**
         * Function to count how many guesses user got right.
         *
         * @function
         */
        function countCorrectGuesses() {
            const cells = document.querySelectorAll("#game-board td");
            let correctGuesses = 0;

            cells.forEach(function (cell) {
                if (cell.classList.contains("correctTile") && cell.classList.contains("selected")) {
                    correctGuesses++;
                }
            });
            return correctGuesses;
        }

        /**
         * Function to store the score in localStorage.
         *
         * @function
         */
        function StoreScore(){
            localStorage.removeItem("score");
            const correctGuesses = countCorrectGuesses();
            const score = getScore(correctGuesses, numSelected, size, difficulty);
            localStorage.setItem("score", JSON.stringify(score));
        }

        // Variable to track if the alert has been shown
        let alertShown = false;

        /**
         * Function that alerts the player's results
         *
         * @function
         */
        function alertPercentage() {
            // Check if the alert has not been shown
            if (!alertShown) { 
                const correctGuesses = countCorrectGuesses();
                const percentage = correctGuesses / correctCounter * 100;
                alert("Your completion percentage was: " + percentage + "%");
                // Set the variable to true to indicate that the alert has been shown
                alertShown = true;
            }
        }

        /**
         * Event listener for clicking the submit button
         * @param {MouseEvent} e - The mouse event object.
         */
        submit.addEventListener("click", StoreScore);

        submit.addEventListener("click", alertPercentage);
    }
    /**
     * Calculates the player's score based on their game performance.
     * @function
     * @param {number} correctGuesses - num of correct Tiles chosen
     * @param {number} numSelected - amount of tiles chosen
     * @param {number} size - size of the game board
     * @param {number} difficulty - difficulty level
     * @returns {number} - The player's score.
     */
    function getScore(correctGuesses, numSelected, size, difficulty){
        const percent = ( 2 * correctGuesses - numSelected) / (size * size);
        return Math.floor(percent * 100 * size * (difficulty + 1));
    }
});