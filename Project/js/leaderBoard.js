"use strict";

/**
 * Function to set up event listeners and handle user interactions for the leaderboard.
 *
 * @function
 */
document.addEventListener("DOMContentLoaded", function () {
    /** @const {Element} */
    const playerNameInput = document.getElementById("playerName");
    const leaderboard = document.querySelector("#leader-board");
    const startGameButton = document.getElementById("start-game");
    const clearButton = document.getElementById("clear");
    const submitButton = document.getElementById("submit");
    const leaderBoardTitle = leaderboard.querySelector("h2");
    /** @type {Element} */
    let scoreCell;
    let num = 1;
    let count = 1;

    /**
     * Event listener for the "Start Game" button. Adds a player to the leaderboard.
     *
     * @function
     */
    startGameButton.addEventListener("click", addPlayerToLeaderboard);

    /**
     * Event listener for the "Clear High Score" button. Clears the leaderboard and local storage.
     *
     * @function
     */
    clearButton.addEventListener("click", clearHighScore);

    /**
     * Event listener for the High score. Reorders to players from lowest to highest.
     *
     * @function
     */
    leaderBoardTitle.addEventListener("click", reorderLeaderboardByAscending);

    /**
     * Function to add a player to the leaderboard.
     *
     * @function
     */
    function addPlayerToLeaderboard() {
        const playerName = playerNameInput.value;

        if (playerName.trim() !== "") {
            const row = document.createElement("tr");

            const playerNameCell = document.createElement("td");
            playerNameCell.textContent = playerName;
            playerNameCell.classList.add("playerName");

            scoreCell = document.createElement("td");
            // Replace with actual scores
            scoreCell.classList.add("score");

            row.appendChild(playerNameCell);
            row.appendChild(scoreCell);
            leaderboard.querySelector("table").appendChild(row);
            playerNameInput.value = "";
            localStorage.setItem(num, playerName);

            /**
             * Event listener for the "Submit" button. 
             * Adds the score to the player in the leaderboard.
             *
             * @function
             */
            submitButton.addEventListener("click", addScore);

            submitButton.addEventListener("click", store);
            /**
            * Event listener for the "Submit" button. calls function reorderLeaderboardByDescending.
            *
            * @function
            */
            submitButton.addEventListener("click", reorderLeaderboardByDescending);
        }
        
    }

    /**
     * Function that stores the last score in local storage.
     * It retrieves the last score from leaderboard and stores it with an
     * incremented key
     * @function
     */
    function store(){
        const scoreElements = document.querySelectorAll(".score");
        if (scoreElements.length > 0) {
            const lastScoreElement = scoreElements[scoreElements.length - 1];
            const lastScore = lastScoreElement.textContent;
            localStorage.setItem(num + " score", lastScore);
            num++;
        }
    }

    /**
     * Function to add the score to the player in the leaderboard.
     *
     * @function
     */
    function addScore() {
        scoreCell.textContent = localStorage.getItem("score");
        count++;
        localStorage.setItem("count", count);
    }

    /**
     * Function to clear the High Score table and local storage.
     *
     * @function
     */
    function clearHighScore(){
        const rows = leaderboard.querySelectorAll("table tr");
        for (let i = 1; i < rows.length; i++){
            rows[i].remove();
        }
        localStorage.clear();
    }
    /**
     * Function to reorder the leaderboard by descending order of scores.
     *
     * @function
     */
    function reorderLeaderboardByDescending() {
        const table = leaderboard.querySelector("table");
        const rows = Array.from(table.querySelectorAll("tr"));

        // Remove the table header
        rows.shift();

        rows.sort((a, b) => {
            const scoreA = parseInt(a.querySelector(".score").textContent);
            const scoreB = parseInt(b.querySelector(".score").textContent);
            return scoreB - scoreA;
        });

        // Append the sorted rows back to the table
        rows.forEach((row) => {
            table.appendChild(row);
        });
    }

    /**
     * Function to reorder the Leaderboard by ascending order of scores.
     *
     * @function
     */
    function reorderLeaderboardByAscending() {
        const table = leaderboard.querySelector("table");
        const rows = Array.from(table.querySelectorAll("tr"));

        // Remove the table header
        rows.shift();

        rows.sort((a, b) => {
            const scoreA = parseInt(a.querySelector(".score").textContent);
            const scoreB = parseInt(b.querySelector(".score").textContent);
            return scoreA - scoreB;
        });

        // Append the sorted rows back to the table
        rows.forEach((row) => {
            table.appendChild(row);
        });
    }


    /**
     * Loop to display player names and scores in the leaderboard.
     *
     * @function
     */
    for (let i = 1; i < 10; i++) {
        const playerNameKey = i.toString();
        const scoreKey = i.toString() + " score";
        const playerName = localStorage.getItem(playerNameKey);
        const playerScore = localStorage.getItem(scoreKey);

        // Check if a player name and score exist for this key
        if (playerName !== null && playerScore !== null) {
            const row = document.createElement("tr");
            const playerNameCell = document.createElement("td");
            playerNameCell.textContent = playerName;
            const scoreCell = document.createElement("td");
            scoreCell.textContent = playerScore;
            scoreCell.classList.add("score");
            row.appendChild(playerNameCell);
            row.appendChild(scoreCell);
            leaderboard.querySelector("table").appendChild(row);
            num++;
        }
        reorderLeaderboardByDescending;

    }

    

    

});