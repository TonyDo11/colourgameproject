"use strict";

/**
 * Function to set up event listeners and handle user interactions.
 *
 * @function 
 */
document.addEventListener("DOMContentLoaded", function () {
    /** @const {Element} */
    const startGameButton = document.getElementById("start-game");
    const submitButton = document.getElementById("submit");
    const playerNameInput = document.getElementById("playerName");
    const numberChoiceInput = document.getElementById("numberChoice");
    const difficultySlider = document.getElementById("difficultySlider");
    const colorChoice = document.getElementById("colorChoice");

    // Initialize a flag to track the completion status
    let allFieldsCompleted = false;

    /**
     * Event listener for input events on the player name field and number choice field.
     * Checks the input fields for validity.
     *
     * @function
     */
    playerNameInput.addEventListener("input", checkFields);
    numberChoiceInput.addEventListener("input", checkFields);
    playerNameInput.reportValidity();
   

    /**
     *Function to check the completion status of the input fields
     * 
     * @function
     */
    function checkFields() {
        const playerName = playerNameInput.value;
        const numberChoice = numberChoiceInput.value;
        if (!playerName) {
            playerNameInput.setCustomValidity("Player Name is required");
        } else if(playerName.length < 5) {
            playerNameInput.setCustomValidity("Player Name must be at least 5 characters");
        } else {
            playerNameInput.setCustomValidity("");
        }

        playerNameInput.reportValidity();
        // Update the completion status flag
        const isPlayerNameValid = playerName.length >= 5;
        const isNumberChoiceValid = !isNaN(numberChoice) && numberChoice >= 3 && numberChoice < 7;
        const isPlayerNameInputValid = playerNameInput.checkValidity();

        allFieldsCompleted = isPlayerNameValid && isNumberChoiceValid && isPlayerNameInputValid;


        // Enable or disable the "Start the game!" button based on the completion status
        startGameButton.disabled = !allFieldsCompleted;
    }

    /**
     * Event listener for the "Start the game!" button. Disables input fields upon start.
     *
     * @function
     */
    startGameButton.addEventListener("click", () => {
        if(allFieldsCompleted) {
            startGameButton.disabled = true;
            playerNameInput.disabled = true;
            numberChoiceInput.disabled = true;
            difficultySlider.disabled = true;
            colorChoice.disabled = true;
        }
        submitButton.disabled = false;
    });

    /** 
     * Event listener for the "Submit" button. Re-enables input fields upon submission.
     *
     * @function
     */
    submitButton.addEventListener("click", () => {
        playerNameInput.disabled = false;
        numberChoiceInput.disabled = false;
        difficultySlider.disabled = false;
        colorChoice.disabled = false;
        submitButton.disabled = true;
    });
  
});
